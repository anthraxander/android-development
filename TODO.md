Features
========

REHEARSAL

Create a band
-------------
* Store contact details for all members
* Store audio files (demos, individual tracks or studio versions)
* Store comprehensive song metadata (time, date, location, session, members, working title, lyrics)
* Provide a shared repository for sharing notes (tagging to match metadata)

Create band communication channel
---------------------------------
* Hook in with existing Facebook group?
* Communicate via SMS (broadcast to all
* In-app communication via the Web

User input
----------
* Ask group questions
* Make requests (gear, follow up lead, chase up other band)
* Vote on issues (where to prac, song names, track orderings)

Record demos
------------
* Track recording
* Track playback
* Track cutting
* Simple waveform inspection (for track cutting)
* Flexible track naming (time stamp format selection, location entry)
* Variable bitrate

Share demos
-----------
* Interface with Soundcloud for cloud-based scratch
* Post to Facebook group page
* E-mail
* Dropbox
* Evernote
* Ask question to band


PERFORMANCE

Track upcoming gigs
-------------------
* Gig calendar for yourself and your scene (hook into FB or songkick?)
* Propose gigs to band
* Vote/express availability