package com.v0id.pbj;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.v0id.pbj.RenderView.RenderThread;

public class AudioRecordActivity extends Activity {
	private static final String LOG_TAG_REC = "Recorder";
	private static final String LOG_TAG_PLY = "Player";
	private static final String LOG_TAG_IO = "I/O";

	private static final String RECORDER_DIR = "PB&J/recording";
	private static final String RECORDER_TEMP_FILE = "scratch.raw";
	private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
	private static final int RECORDER_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
	private static final int STOP_PLAYBACK = 1;
	private static final int STOP_RECORDING = 2;

	// recording objects
	private static AudioRecord aRecorder = null;
	private static int aRecorderRate;
	private static int bufferSize;
	private static byte[][] audioData = new byte[32][128];
	private static int bufferId = 0;
	private static AudioTrack aTrack = null;
	private Thread recordingThread = null;
	private Thread playbackThread = null;
	private boolean isRecording = false;
	private boolean isPlaying = false;

	String tempName = null;
	String fileName = null;
	
    // UI elements
	private ImageButton masterButton = null;
	private Button saveButton = null;
	private Button deleteButton = null;
	
	// handles to drawing thread
	private RenderThread hRenderThread;
	private RenderView hRenderView;

	// TODO: File type selection Spinner
	// TODO: Test .wav header
	// TODO: Graphical waveform of staged file
	// TODO: Affix timestamp to filename
	// TODO: Improve design in `record.xml`

	/*
	 * Recording thread, using loopback to stream audio
	 */
	private void startRecording() {
		recordingThread = new Thread(new Runnable() {
			@Override
			public void run() {
				android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
				doRecording();
			}
		}, "AudioRecord thread");
		recordingThread.start();
	}

	/*
	 * Loopback recording method
	 */
	private void doRecording() {
		bufferSize = AudioRecord.getMinBufferSize(aRecorderRate,
				RECORDER_CHANNELS, RECORDER_ENCODING);
		System.out.println(bufferSize);
		Log.d(LOG_TAG_REC, "Buffer size: " + Integer.toString(bufferSize));
        
		aRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
				aRecorderRate, RECORDER_CHANNELS,
				RECORDER_ENCODING, bufferSize);
		Log.d(LOG_TAG_REC, Integer.toString(aRecorder.getState()));

		aTrack = new AudioTrack(3, aRecorderRate,
				AudioFormat.CHANNEL_OUT_MONO, RECORDER_ENCODING,
				bufferSize, 1);
		
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(getTempFile());
    		aRecorder.startRecording();
        	Log.i(LOG_TAG_REC, "Recording started");
        	
        	isRecording = true;
        	while (isRecording) {
        		bufferId++;
        		bufferId = bufferId % audioData.length;
        		aRecorder.read(audioData[bufferId], 0, audioData[bufferId].length);
        		try {
        		    out.write(audioData[bufferId]);
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
        	}
        	out.close();
    		updateButton.obtainMessage(STOP_RECORDING).sendToTarget();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void stopRecording() {
		if (aRecorder != null) {
			isRecording = false;
			aRecorder.stop();
			aRecorder.release();
			aRecorder = null;
			recordingThread = null;
			// Track staged, enable save and delete options
			saveButton.setEnabled(true);
			deleteButton.setEnabled(true);
			Log.i(LOG_TAG_REC, "Recording stopped");
		}
	}

	private File getTempFile() {
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
		    String filepath = Environment.getExternalStorageDirectory().getPath() + "/" + RECORDER_DIR;
		    File file = new File(filepath, RECORDER_TEMP_FILE);
	    	file.getParentFile().mkdirs();
		    return file;
		}
		return null;
	}

	private void saveFile() {
		FileInputStream in = null;
		FileOutputStream out = null;
		byte[] audioData = new byte[bufferSize];

		// Get storage directory
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File file = new File(filepath, RECORDER_DIR);
		filepath = file.getAbsolutePath();

		// Get filename from user input, or set default
		EditText txtTitle = (EditText) findViewById(R.id.txtTitle);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String date = dateFormat.format(new Date());
		if (txtTitle.getText().toString().trim().length() > 0) {
			// TODO: Use regEx to clean up user input
		    fileName = date + "_" + txtTitle.getText().toString() + ".raw";
		} else {
			// TODO: Configure default filename at beginning of recording
		    fileName = date + ".raw";
		}

		fileName = filepath + "/" + fileName;
		Log.i(LOG_TAG_IO, "Trying to save file to: " + fileName);

		try {
			in = new FileInputStream(getTempFile());
			out = new FileOutputStream(fileName);
			long totalLength = in.getChannel().size();
			Log.i(LOG_TAG_IO, "File size: " + totalLength);

			while (in.read(audioData) != -1)
				out.write(audioData);

			in.close();
			out.close();
			Log.i(LOG_TAG_IO, "Saved file to " + fileName);
		} catch (FileNotFoundException e) {
			Log.i(LOG_TAG_IO, "Failed save to file (file not found)");
		} catch (IOException e) {
			Log.i(LOG_TAG_IO, "Failed save to file");
		}
	}

	private void deleteTempFile() {
		File tempFile = getTempFile();
		// Delete scratch file
		if (tempFile.exists())
			tempFile.delete();
		// Clear AudioTrack
		aTrack.flush();
		aTrack.release();
		aTrack = null;
	}

	private void startPlaying() {
		playbackThread = new Thread(new Runnable() {
			@Override
			public void run() {
				doPlaying();
			}
		}, "AudioPlayback thread");
		playbackThread.start();
	}
	
	private void doPlaying() {
		if (aTrack != null) {
			File file = getTempFile();
			FileInputStream in = null;
			try {
				in = new FileInputStream(file);
    			int count = 512 * 1024;
    			int bytesRead = 0;
    			int size = (int) file.length();
    			byte[] data = new byte[count];
    			aTrack.setPlaybackRate(aRecorderRate);
    			aTrack.play();
    			isPlaying = true;
    			Log.i(LOG_TAG_PLY, "Started playback");
    			int ret;
    			while (bytesRead < size) {
    				ret = in.read(data, 0, count);
    				if (ret != -1) {
    					bytesRead += ret;
    					aTrack.write(data, 0, ret);
    				} else {
    					break;
    				}
    			}
    			in.close();
    			updateButton.obtainMessage(STOP_PLAYBACK).sendToTarget();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void stopPlaying() {
		if (aTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
			aTrack.stop();
			isPlaying = false;
			Log.i(LOG_TAG_PLY, "Stopped playback");
   			masterButton.setBackgroundResource(R.drawable.play);
		}
	}
	
	public final Handler updateButton = new Handler() {
       @Override
       public void handleMessage(Message msg) {
       if (msg.what == STOP_PLAYBACK)
           stopPlaying();
       else if (msg.what == STOP_RECORDING)
           stopRecording();
       }
    };


	/*
	 * Master button listener
	 */
	private class MasterAction implements Button.OnClickListener {
		@Override
		public void onClick(View v) {
			if (aTrack == null) {
				startRecording();
				masterButton.setBackgroundResource(R.drawable.stop);
			} else if (isRecording) {
				stopRecording();
				masterButton.setBackgroundResource(R.drawable.play);
			} else if (isPlaying) {
				stopPlaying();
			} else {
				startPlaying();
				masterButton.setBackgroundResource(R.drawable.stop);
			}
		}
	}

	/*
	 * Save button Listener
	 */
	private class SaveAction implements Button.OnClickListener {
		@Override
		public void onClick(View v) {
			if (aTrack == null) {
				return;
			} else {
				saveFile();
			}
		}
	}

	/*
	 * Clear button Listener
	 */
	private class DeleteAction implements Button.OnClickListener {
		@Override
		public void onClick(View v) {
			if (aTrack == null) {
				return;
			} else {
				deleteTempFile();
				saveButton.setEnabled(false);
				deleteButton.setEnabled(false);
				masterButton.setBackgroundResource(R.drawable.rec);
        		EditText txtTitle = (EditText) findViewById(R.id.txtTitle);
				txtTitle.setText("");
			}
		}
	}

	/*
	 * Sample rate spinner listener
	 */
	private class QualityAction implements Spinner.OnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int pos, long id) {
			switch (pos) {
			case 0:
				aRecorderRate = 44100;
				break;
			case 1:
				aRecorderRate = 22050;
				break;
			case 2:
				aRecorderRate = 16000;
				break;
			case 3:
				aRecorderRate = 8000;
				break;
			}
			Log.i(LOG_TAG_REC, "Sample rate: " + aRecorderRate);
		}

		public void onNothingSelected(AdapterView<?> parent) {
			aRecorderRate = 8000;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.record);
		// Get handles to the RenderView
		hRenderView = (RenderView) findViewById(R.id.render);
		hRenderThread = hRenderView.getThread();
		
		Spinner spnBitrate = (Spinner) findViewById(R.id.spnBitrate);
		// Create an adapter using an array of strings and default layout
		ArrayAdapter<CharSequence> adapterBitrate = ArrayAdapter.createFromResource(this,
				R.array.spnBitrate, android.R.layout.simple_spinner_item);
		adapterBitrate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply adapter to the spinner
		spnBitrate.setAdapter(adapterBitrate);
		// Set listener
		QualityAction qualityListener = new QualityAction();
		spnBitrate.setOnItemSelectedListener(qualityListener);

		// As above
		Spinner spnType = (Spinner) findViewById(R.id.spnType);
		ArrayAdapter<CharSequence> adapterType = ArrayAdapter.createFromResource(this,
				R.array.spnType, android.R.layout.simple_spinner_item);
		adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnType.setAdapter(adapterType);
		// TODO: Implement recording type spinner listener

		// Configure button listeners
		MasterAction masterListener = new MasterAction();
		masterButton = (ImageButton) findViewById(R.id.btnMaster);
		masterButton.setOnClickListener(masterListener);
		SaveAction saveListener = new SaveAction();
		saveButton = (Button) findViewById(R.id.btnSave);
		saveButton.setEnabled(false);
		saveButton.setOnClickListener(saveListener);
		DeleteAction deleteListener = new DeleteAction();
		deleteButton = (Button) findViewById(R.id.btnDelete);
		deleteButton.setEnabled(false);
		deleteButton.setOnClickListener(deleteListener);
	}
}
