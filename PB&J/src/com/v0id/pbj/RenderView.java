package com.v0id.pbj;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class RenderView extends SurfaceView implements SurfaceHolder.Callback {
	public RenderView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	class RenderThread extends Thread {
		private static final String LOG_TAG_RENDER = "Renderer";
		private SurfaceHolder hSurfaceHolder;
		private Handler hHandler;
		private Context hContext;
		private boolean hRun = false;
		private final Object hLock = new Object();
		private int canvasWidth = 1;
		private int canvasHeight = 1;

		public RenderThread(SurfaceHolder surfaceHolder, Context context, Handler handler) {
			hSurfaceHolder = surfaceHolder;
			hHandler = handler;
			hContext = context;
			// TODO initialise state
		}
		
		public void doStart() {
			synchronized (hSurfaceHolder) {
				DisplayMetrics displayMetrics = new DisplayMetrics();
				((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
				int width = displayMetrics.widthPixels;
				Log.d(LOG_TAG_RENDER, "Width: " + Integer.toString(width));
				int height = displayMetrics.heightPixels;
				Log.d(LOG_TAG_RENDER, "Height: " + Integer.toString(height));
				// TODO calculate size and placement of canvas
			}
		}
		
		public void setRunning(boolean b) {
			synchronized (hLock) {
				hRun = b;
			}
		}
		
		@Override
		public void run() {
			while (hRun) {
				// TODO get a Canvas
				Canvas c = null;
				try {
					c = hSurfaceHolder.lockCanvas(null);
					synchronized (hSurfaceHolder) {
						// TODO critical section:
						Paint hPaint = new Paint();
						hPaint.setColor(Color.BLUE);
						hPaint.setStyle(Paint.Style.FILL);
						c.drawPaint(hPaint);
					}
					synchronized (hLock) {
						if (hRun) doDraw(c);
					}
				} finally {
					if (c != null) {
						hSurfaceHolder.unlockCanvasAndPost(c);
					}
				}
			}
		}
		
		private void doDraw(Canvas canvas) {
			// TODO reduce data to n samples for n drawable pixels
			// TODO get min, max within each sample
			// TODO draw line between these values
			return;
		}
	}
	
	private RenderThread thread;
	public RenderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);
		
		thread = new RenderThread(holder, context, new Handler());
	}
	
	public RenderThread getThread() {
		return thread;
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// start that shit
		thread.setRunning(true);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
	}
}
