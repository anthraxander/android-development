package com.v0id.pbj;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

public class Encoder {
	public Encoder() {
		// default constructor
	}
	
	public void writeWAV(File file, String outpath, int channels, int bitRate) {
		try {
		    // TODO: Write method to get raw data from file
		    byte[] data = readFile(file);
		    
		    long chunkSize1 = 16; // 16 for PCM
		    long chunkSize2 = data.length;
		    long chunkSize = 36 + chunkSize2;
		    
		    int audioFormat = 1; // 1 for PCM (linear quantization)
		    long numChannels = Long.valueOf(channels);
		    long sampleRate = Long.valueOf(bitRate);
		    int bitsPerSample = 16; // 16 or 8
		    long byteRate = sampleRate * numChannels * bitsPerSample / 8;
		    int blockAlign = (int) (numChannels * bitsPerSample / 8);
		    
		    OutputStream os = null;
		    os = new FileOutputStream(new File(outpath + file.getName() + ".wav"));
		    BufferedOutputStream bos = new BufferedOutputStream(os);
		    DataOutputStream out = new DataOutputStream(bos);
		    
		    // Write byte arrays to output file
		    out.writeBytes("RIFF");									// 00 - RIFF header
		    out.write(intToBytes((int)chunkSize), 0, 4);			// 04
		    out.writeBytes("WAVE");									// 08
		    out.writeBytes("fmt ");									// 12 - format header
		    out.write(intToBytes((int) chunkSize1), 0, 4);			// 16
		    out.write(shortToBytes((short) audioFormat), 0, 2);		// 20
		    out.write(shortToBytes((short) numChannels), 0, 2);		// 22
		    out.write(intToBytes((int) sampleRate), 0, 4);			// 24
		    out.write(intToBytes((int) byteRate), 0, 4);			// 28
		    out.write(shortToBytes((short) blockAlign), 0, 2);		// 32
		    out.write(shortToBytes((short) bitsPerSample), 0, 2);	// 34
		    out.writeBytes("data");									// 36 - data header
		    out.write(intToBytes((int) chunkSize2), 0, 4);			// 40
		    out.write(data);										// 44 - raw data
		    
		    out.flush();
		    out.close();
		    
		    System.out.println(file.getName() + " written in .wav format");
		} catch (Exception e) {	}
	}

	private static final byte[] intToBytes(int value) {
	    return new byte[] {
		            (byte) (value >>> 24),
		            (byte) (value >>> 16),
		            (byte) (value >>> 8),
		            (byte) value
	            };
	}

	private static final byte[] shortToBytes(short s) {
	        return new byte[]{
		        		(byte) (s & 0x00FF),
		        		(byte) ((s & 0xFF00) >> 8)
	        		};
	}
	
	/*
	 * Read audio file to byte[], avoids use of external libs
	 */
	private static final byte[] readFile(File file) throws IOException {
		RandomAccessFile f = new RandomAccessFile(file, "r");
		try {
			long longLength = f.length();
			int length = (int) longLength;
			if (length != longLength)
				throw new IOException("File size is > 2GB");
			byte[] data = new byte[length];
			f.readFully(data);
			return data;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			f.close();
		}
		return null;
	}
}
